<h1>CSS Repeated Headers and Footers</h1>

<pre class='metadata'>
ED: https://specs.rivoal.net/css-repeat/
Shortname: css-repeat
Level: 1
Status: DREAM
Status Text: ...
Repository: https://gitlab.com/frivoal/specs.git gitlab
Issue Tracking: GitLab https://gitlab.com/frivoal/specs/issues
Issue Tracker Template: https://gitlab.com/frivoal/specs/issues/{0}
Work Status: Exploring
Editor: Florian Rivoal, https://florian.rivoal.net/
Abstract: CSS defines that table headers and footers are repeated when the table is fragmented.
 This specification defines a generalization of this mechanism,
 enabling authors to use it on elements other than tables.
</pre>

<h2 id=intro>
Introduction</h2>
<em>This section is non-normative</em>.

A useful feature of the header and footer of [[HTML]] <{table}>
has traditionally not fully been explain by CSS:
when the table is fragmented across several pages or columns,
the header and footer is repeated on each fragment.

[[CSS2]] and later [[CSS-TABLES-3]] have given some explanation about how this works,
but do so by linking this behavior to the <a>display type</a>.

This specification instead explains it with a dedicated 'repeat-on-break' property,
and defines it in greater detail.
It's initial ''repeat-on-break/auto'' value triggers the legacy behavior of repeating the table header and footer,
and setting it explicitly enables authors to get this effect on other elements,
or to turn it off on table headers and footers where this is not desired.

Note: Fragmentation related behaviors,
including repetition of table headers when fragmenting,
have historically been better supported by print-oriented User Agents
than by deskop or mobile browsers,
as they attach greater importance to pagination.
However, such features are by no means intended to be exclusive to print-oriented UAs,
and support in browsers in encouraged.

<h3 id="motivation">
Motivating scenarios</h3>

In no particular order,
here are a few examples of situations where this property can be useful:

* On a table with multiple headers or footers, to select which one gets repeated.
* To turn off repetition for some tables when the effect is not desired.
* To repeat the caption of a figure when the figure is fragmented.
    <span class=note>Note: This would become even more useful if combined with a fragment specific selector
    to insert “(continued)” or some similar phrasing after the repeated caption.</span>
* It is occasionally useful to style a <{table}> using some other <a>display type</a>,
    such as those defined in [[CSS-FLEXBOX-1]] or [[CSS-GRID-1]].
    This property makes it possible to do so
    while keeping the ability to repeat relevant parts of the table when fragmenting.
* When pseudo elements  like ''::before'' and ''::after'' are used for decorative purposes,
    it can be desirable to repeat them in case of  fragmentation.

<h2 id="interaction">Module Interactions</h2>
This document allows authors to opt in or out of a behavior
that previous specifications defined to apply to some elements (table headers and footers) only.
It therefore replaces and supersedes the parts of previous specifications
that defined this behavior:
* The following statements in <a href="https://www.w3.org/TR/CSS2/tables.html#table-display">Section 17.2</a> of [[CSS2]]:
    * <blockquote>Print user agents may repeat header rows on each page spanned by a table. If a table contains multiple elements with 'display: table-header-group', only the first is rendered as a header; the others are treated as if they had 'display: table-row-group'. </blockquote>
    * <blockquote>Print user agents may repeat footer rows on each page spanned by a table. If a table contains multiple elements with 'display: table-footer-group', only the first is rendered as a footer; the others are treated as if they had 'display: table-row-group'. </blockquote>
* [[css-tables-3#repeated-headers]]

Note: This specification is not intended to contradict the behavior described in [[CSS-TABLES-3]], only to generalize it.
Leaving the 'repeat-on-break' property at its initial value is expected to
produce the same behavior as the one described in [[css-tables-3#repeated-headers]].
If it does not this should be considered an error in this document.
If you notice any such problem, please report it.

<h2 id=repeat-on-break>
Repeating elements: the 'repeat-on-break' property</h2>

This property enables authors to designate one child of an element
as the <a>repeatable header</a> and one as the <a>repeatable footer</a> of that element.
If this element is <a spec=css-break>fragmented</a>,
they will be <a href="#repetition">repeated</a> at the beginning and end of each <a spec=css-break>fragment</a>.
Beforehand,
the children of that parent are <a href="#reordering">reordered</a> to make the <a>repeatable header</a> and <a>repeatable footer</a> first and last.

<pre class=propdef>
 Name: repeat-on-break
 Value: auto | none | header | footer
 Initial: auto
 Inherited: No
 Applies to:  <a>Block-level</a> elements, <a spec="css-tables">table-row-grouping</a> elements, <a>flex items</a>, <a>grid items</a>
 Computed value: See below for ''repeat-on-break/auto'', as specified for other values
 Animation type: discrete
</pre>

Values have the following meanings:

<dl>
	<dt><dfn dfn-for="repeat-on-break" dfn-type=value>auto</dfn>
	<dd>The ''repeat-on-break/auto'' value computes to:
	* ''repeat-on-break/header'' when the computed value of the 'display' property is  is ''display/table-header-group''
	* ''repeat-on-break/footer'' when the computed value of the 'display' property is ''display/table-footer-group''
	* ''repeat-on-break/none'' otherwise

	<dt><dfn dfn-for="repeat-on-break" dfn-type=value>none</dfn>
	<dd>The element follows normal <a>fragmentation</a> rules, as defined in [[!CSS-BREAK-3]]

	<dt><dfn dfn-for="repeat-on-break" dfn-type=value>header</dfn>
	<dd>The first child (in document order) of an element to have a computed value of ''header''
	becomes the <dfn export>repeatable header</dfn> of that element.
	''header'' on subsequent siblings has no effect.

	<dt><dfn dfn-for="repeat-on-break" dfn-type=value>footer</dfn>
	<dd>The first child (in document order) of an element to have a computed value of ''footer''
	becomes the <dfn export>repeatable footer</dfn> of that element.
	''footer'' on subsequent siblings has no effect.
</dl>

Issue: An alternative approach would be
to have the property apply to the parent rather than on the header and footer directly,
and turn its first / last childern into a header / footer.
The syntax would the become: ''auto | none | header || footer''.
This would simplify how this works by getting rid of reordering,
and by reducing the amount of tree traversal needed to make this work.
However, something else would then need to explain
how the first <a spec=css-tables>table-header-group</a> and <a spec=css-tables>table-footer-group</a> get reordered,
since they do have this behavior and it is currently underspecified.
We would also need to define the interaction with the 'order' property
on <a>flex items</a> and <a>grid items</a>:
presumably, just like with tables,
we'd have to repeat the first/last child after reordering,
which negates some of the tree traversal simplifications.
Also, we would have to deal with situation
where the first/last child of an elements
are not <a>Block-level</a> elements, <a spec="css-tables">table-row-grouping</a> elements, <a>flex items</a>, or <a>grid items</a>.
Should we still reorder in that case, or should this make the property have no effect?

Issue: Add examples

<h3 id=reordering>Reordering</h3>

For the purpose of layout, if an element has a <a>repeatable header</a> child,
that child is treated as if it was the first child of the element,
and if it has a <a>repeatable footer</a> child,
that child is treated as if it was the last child of the element.
This also affects the painting order defined
in [[css-flexbox-1#painting]] for <a>flex items</a>,
in [[css-grid-1#z-order]] for <a>grid items</a>,
and in <a href="https://www.w3.org/TR/CSS2/zindex.html">Appendix E of CSS2</a> for other elements.

For <a>flex items</a> and <a>grid items</a>, the effect is identical to that of the 'order' property;
<a>repeatable headers</a> behave is if they had the smallest 'order' of all their siblings,
<a>repeatable footers</a> as if they had the largest one.

This reordering <em>does not</em> affect ordering in non-visual media (such as speech),
nor does it affect the default traversal order of sequential navigation modes
(such as cycling through links, see e.g. <a href="https://www.w3.org/TR/html5/editing.html#sequential-focus-navigation-and-the-tabindex-attribute"><code>tabindex</code></a> [[HTML5]]).

<div class=note>
  This choice of affecting ordering only for layout and painting is for the sake of:
    * Compatibility with  <a spec=css-tables>table-header-group</a> and <a spec=css-tables>table-footer-group</a> which already have this behavior
    * Consistency with the 'order' property on flebox and grid, which behave that way as well.
</div>

Advisement: As with reordering using the 'order' property,
authors must only use ''repeat-on-break/header'' (resp. ''repeat-on-break/footer'') on elements that are
not the first (resp. last) child
when the visual order needs to be out-of-sync with the speech and navigation order;
otherwise the underlying document source should be reordered instead.
See [[css-flexbox-1#order-accessibility]].

<h3 id=repetition>Repetition</h3>

If an element with a <a>repeatable header</a> is <a spec=css-break>fragmented</a>,
the UA should repeat the <a>repeatable header</a> at the beginning of each fragment.

If an element with a <a>repeatable footer</a> is <a spec=css-break>fragmented</a>,
the UA should repeat the <a>repeatable footer</a> at the end of each fragment,
pushing content the next <a>fragmentainer</a> in the <a>fragmentation context</a>
as necessary to make room for the <a>repeatable footer</a>.

Issue: Should this also apply to wrapped rows of a ''flex-wrap: wrap'' element in addition to fragmentation?

To account for the various rules and properties creating or suppressing break opportunities,
to account for the prioritization of different classes of break-points,
to avoid overflow or excessive fragmentation,
or to strike an appropriate balance between performance and quality of layout,
the UA may skip either or both of the <a>repeatable header</a> or the <a>repeatable footer</a> in some fragmentainers.

The exact algorithm to decide in with case to repeat or skip
either or both of the <a>repeatable header</a> or the <a>repeatable footer</a> is up to the UA.
However, the UA must guarantee progress through the content
while ensuring that none of the content (including the repeatable header and footer) gets dropped.
For that purpose, the algorithm must at least fulfil the following constraints:
* If  <a>repeatable header</a> or a <a>repeatable footer</a> is too large to fit in its <a>fragmentainer</a>
    and if it is not being skipped,
    whether it overflows or is fragmented is determined by the usual rules of <a spec=css-break>fragmentation</a>.
* The UA must include the <a>repeatable header</a> at least once, at the beginning of the first fragment of it parent.
    If this first instance of the <a>repeatable header</a> is itself fragmented,
    the UA must not include any further content (including the <a>repeatable footer</a>
    until all the fragments of the <a>repeatable header</a> have been placed.
* The UA must include <a>repeatable footer</a> at least once, at the end of the last fragment of its parent.
* Other than when placing the first <a>repeatable header</a> and the last <a>repeatable footer</a>,
    if a <a>fragmentainer</a> is too small to fit any content other than the <a>repeatable header</a> and <a>repeatable footer</a>,
    then the UA must skip either or both of them to make room for the content.

In addition, in order to guarantee backwards compatibility with the behavior of tables in desktop browsers:

: If
::
       * the <a>fragmentainer</a> is a page, <strong>and</strong>
       * the computed value of the 'display' property on the <a>repeatable header</a> (resp. <a>repeatable footer</a>) is ''display: table-header-group'' (resp. ''display: table-footer-group''), <strong>and</strong>
       * the computed value of the 'break-inside' property on the <a>repeatable header</a> (resp. <a>repeatable footer</a>) is ''break-inside: avoid'', <strong>and</strong>
       * the <a>logical height</a> of the <a>repeatable header</a> (resp. <a>repeatable footer</a>)  when laid out in that <a>fragmentainer</a> would be equal to or smaller than 25% of the <a>logical height</a> of the <a>page area</a>.
: then
:: the <a>repeatable header</a> (resp. <a>repeatable footer</a>) must not be skipped.

Advisement: A UA that would only include the <a>repeatable header</a> and <a>repeatable footer</a>
at the beginning, the end, and the positions required for compability with the behavior of tables in desktop browsers
and skip them everywhere else
would not directly violate the above constraints.
Such an implementation would not however be useful,
as it would bring no new capability to authors
other than <a href="#reordering">reordering</a>,
which is merely a side effect, not the main point of this feature.
Implementations which only support the ''repeat-on-break: none'' and ''repeat-on-break: auto'' values
may limit themselves to this minimalist interpretation.
However, implementations must not support the ''repeat-on-break: header'' and ''repeat-on-break: footer'' values
without making the <a>repeatable header</a> and <a>repeatable footer</a> actually repeat
with display types other than ''display: table-header-group'' and ''display: table-footer-group''.
While the flexibility given to implementors makes this difficult to assertain via formal tests,
ignoring this requirement would make an implementation non-conforming.

Issue: Should we try to specify repeat/skip the algorithm instead of leaving it up to the UA?
Should we leave it up to the UA but more constraints on it?

<h2 class="no-num" id="security_and_privacy"> Apendix A. 
Security and Privacy Considerations</h2>

<em>This appendix is non-normative</em>.

There are no known security or privacy impacts of this feature. 

The W3C TAG is developing a
<a href="https://w3ctag.github.io/security-questionnaire/">Self-Review Questionnaire: Security and Privacy</a>
for editors of specifications to informatively answer.
As far as currently known, here are the answers to the <a href="https://w3ctag.github.io/security-questionnaire/#questions">Questions to Consider</a>:

<dl>
<dt>Does this specification deal with personally-identifiable information?
<dd>No

<dt>Does this specification deal with high-value data?
<dd>No.

<dt>Does this specification introduce new state for an origin that persists across browsing sessions?
<dd>No.

<dt>Does this specification expose any other data to an origin that it doesn’t currently have access to?
<dd>No.

<dt>Does this specification enable new script execution/loading mechanisms?
<dd>No.

<dt>Does this specification allow an origin access to a user’s location?
<dd>No.

<dt>Does this specification allow an origin access to sensors on a user’s device?
<dd>No.

<dt>Does this specification allow an origin access to aspects of a user’s local computing environment?
<dd>No.

<dt>Does this specification allow an origin access to other devices?
<dd>No.

<dt>Does this specification allow an origin some measure of control over a user agent’s native UI?
<dd>No.

<dt>Does this specification expose temporary identifiers to the web?
<dd>No.

<dt>Does this specification distinguish between behavior in first-party and third-party contexts?
<dd>No.

<dt>How should this specification work in the context of a user agent’s "incognito" mode?
<dd>No difference in behavior is expected or needed.

<dt>Does this specification persist data to a user’s local device?
<dd>No.

<dt>Does this specification have a "Security Considerations" and "Privacy Considerations" section?
<dd>Yes, this is the role of this Appendix.

<dt>Does this specification allow downgrading default security characteristics?
<dd>No.
</dl>

