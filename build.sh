#!/bin/sh
set -e

OUT=_site
MD=markdown_py
SPECS=`ls */Overview.bs | sed -e "s/\/.*$//"`

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi
cp -r $SPECS $OUT/
cp googlee36889053781b1d9.html $OUT/

for i in $SPECS ; do
	bikeshed spec $OUT/$i/Overview.bs
	rm $OUT/$i/Overview.bs
done

echo "<!doctype html>" > $OUT/index.html
echo "<html lang=en>" >> $OUT/index.html
echo "<meta charset=utf-8>" >> $OUT/index.html
echo "<meta name='viewport' content='width=device-width, initial-scale=1'>" >> $OUT/index.html
echo "<title>`grep "#.*" README.md | head -1 |sed -e "s/# *//"`</title>" >> $OUT/index.html
echo "<style>body { margin: 3em auto; max-width: 30em; line-height: 1.3; } p { margin: 0; text-indent: 1em; text-align: justify; hyphens: auto; }</style>" >> $OUT/index.html
$MD README.md >> $OUT/index.html
echo >> $OUT/index.html
echo "<ul>" >> $OUT/index.html
for i in $SPECS ; do
	echo "	<li><a href='./$i/'>$i</a>" >> $OUT/index.html
done
echo "</ul>" >> $OUT/index.html
