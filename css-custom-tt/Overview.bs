<h1>CSS Custom Text transformations</h1>

<pre class='metadata'>
ED: https://specs.rivoal.net/css-custom-tt/
Shortname: css-custom-tt
Level: 1
Status: DREAM
Status Text: ...
Repository: https://gitlab.com/frivoal/specs.git gitlab
Issue Tracking: GitLab https://gitlab.com/frivoal/specs/issues
Issue Tracker Template: https://gitlab.com/frivoal/specs/issues/{0}
Work Status: Exploring
Editor: Florian Rivoal, https://florian.rivoal.net/
Abstract: This specification defines a mechanism for authors to create custom values for the CSS 'text-transform' property.
</pre>
<pre class=link-defaults> block:
spec:css-syntax-3; type:type; text:<urange>
</pre>

<h2 id=intro>
Introduction</h2>
<em>This section is non-normative</em>.


<h3 id="motivation">
Motivating scenarios</h3>

<h2 id="interaction">Module Interactions</h2>

<h2 id=at-tt>
Defining Custom Text transformations: the ''@text-transform'' rule</h2>

The general form ''@text-transform'' rule is:
<pre class=prod>
<dfn dfn-type=at-rule>@text-transform</dfn> <<transform-name>> {
	<<declaration-list>>
}
<dfn dfn-type=type><<transform-name>></dfn> = <<custom-ident>>
</pre>

The ''@text-transform'' rule accepts the following descriptors:
''@text-transform/transform'' and ''@text-transform/position''.
The ''@text-transform/transform'' descriptor is required,
and the ''@text-transform'' rule has no effect if it is omitted.
Other descriptors may be omitted,
and evaluate to their initial value in that case.
When a given descriptor occurs multiple times in a given ''@text-transform'' rule,
only the last one is used;
all prior instances of that descriptor within that rule must be ignored.

These descriptors apply solely within the context of the enclosing ''@text-transform,
and do not apply to document elements.

Issue: define what happens when multiple Custom Text transformations by the same name are declared.

The 'text-transform' property is extended by this specification to accept
Customer Text transformations, refered to by their <<transform-name>>, as values:
<pre class="propdef partial">
 Name: text-transform
 New Values: none | capitalize | uppercase | lowercase | full-width | <<transform-name>>
</pre>

With the exception of <a>CSS-wide keywords</a> which always have their global meaning,
if <<transform-name>> conflicts with an existing value of the 'text-transform' property,
the conflict is resolved in favor of the custom definition introduced using '' @text-transform''.

Note: This enables the addition by later specifications of new keywords for the 'text-transform' property
without changing the behavior of any existing page already defining and using a custom text transformation by the same name.

<h3 id=transform-desc>
Defining the transformation: '@text-transform/transform' descriptor</h3>

<pre class='descdef'>
 Name: transform
 Value: <<conversion>>#
 Initial: N/A
 For: @text-transform
</pre>

<pre class=prod>
<dfn dfn-type=type><<conversion>></dfn> = [<<char-list>> to <<char-list>>] | <<'text-transform'>>
<dfn dfn-type=type><<char-list>></dfn> = <<enumeration>> | <<range>>
<dfn dfn-type=type><<range>></dfn> = <<urange>> | <<string>>
<dfn dfn-type=type><<enumeration>></dfn> = <<string>>
</pre>

This descriptor defines which character will be replaced by which,
by listing a series of conversions,
to be applied in the same order as they appear in the descriptor.

Conversions may refer to existing text transformations,
either predefined by CSS or defined by the author.

Note: While a transformation using only a single such conversion is not very useful,
combining it with other conversions allows authors to extend or define variants of existing transformations.

Referring to the text-transform currently being defined is not allowed,
and makes the whole descriptor invalid.

Conversions may also define new mapping from one <<char-list>> to another.
When defined using a <<urange>>,
the <<char-list>> is is an ordered list of each individual Unicode character code point designated by the <<urange>>.

A <<range>> may also be defined as a string made of a single unicode character,
followed by a hyphen (U+002D),
followed by another signle unicode character.
The semantics are identical to the <<urange>> ''U+XXXXXX-YYYYYY''
where XXXXXX is the code point of the first character
and YYYYYY the code point of the second character.
Any other string is interpreted as an <<enumeration>>.

Note: This notation is included despite being redundant with the <<urange>> notation as is is much
more readable in many situations.

Issue: Since these operate on single code points, which normalization (if any) is used matters.
Should we have a way to switch how they apply?
Should we have built-in nfc, nfd, nfkc and nfkd 
transforms?

If defined by an <<enumeration>>,
the <<char-list>> is an ordered list of each <a>extended combining character squence</a> in the string,
The same character may not appear twice in the <<char-list>> defining the source of the mapping,
otherwise the whole descriptor is invalid.

Note: In addition to the usual CSS rules of character escaping,
hyphen (U+002D) need to be escaped to appear in second position of a 3 charater long <<enumeration>>.
The string would otherwise be interpreted as a <<range>>.

Issue: This ability to have both enumeration strings and range strings is nice for authors,
but is this something implementors are willing to live with,
or does it make parsing too cumbersome?
Wrapping the strings in functional notation would solve the problem,
at the expense of making the syntax much more verbose and less readable.

In a <<conversion>>, if the source <<char-list>> is longer than the target <<char-list>>,
then the last item of the target list is used for all remaining items in the source list.

Note: This allows shortcuts like <code highlight=css>transform: "aàáå" to "a"</code>.

Issue: We probably need a switch of some kind to be able to
operate only on base characters.
Maybe also only on combining marks, or sets of combining marks.
At least for base characters only, there are clear use cases.

Issue: Should we allow spaces and other collapsible characters in the target? Since text-transform is applied after white space collapsing, what are the implications of generating runs of collapsible white space that won't be collapsed? It has been proposed that we should allow them, and trigger a second white space collapsing if they are actually used.

Issue: Should we allow an empty <<char-list>> as the target? It has been suggested that this be used to delete text. I am not sure I like the idea that text-transform could be able to make some non-empty element empty.

Issue: It has been suggested that it should be possible to write text-transforms that behave differently on different languages. This can probably be achieved by adding some optional part at the beginning of each <<conversion>>, although I am not sure what the syntax should be.

<div class=example>
<pre highlight=css>
@text-transform latin-only-uppercase {
	transform: "a-z" to "A-Z";
}
</pre>
</div>

<div class=example>
 The following two transformations are identical.

<pre highlight=css>
@text-tranform abcdef1 {
	transform: "abc" to "def";
}
@text-tranform abcdef2 {
	transform: "a" to "d",
	           "b" to "e",
	           "c" to "f";
}
</pre>
</div>

<h3 id=position-desc>
What the transformation applies to: the 'position' descriptor</h3>

<pre class='descdef'>
 Name: position
 Value: all | [ initial || medial || final ]
 Initial: all
 For: @text-transform
</pre>

 This descriptor makes it possible to restrict
 which characters in the source text
 are affected by the transform.

<ul dfn-for="@text-transform/position" dfn-type="value">
	<li><dfn>all</dfn> lets the transformation apply to any character
	<li><dfn>initial</dfn> lets the transformation apply at the beginning of a word
	<li><dfn>medial</dfn> lets the transformation apply to characters within a word other than at the beginning and the end.
	<li><dfn>final</dfn> lets the transformation apply at the end of a word
</ul>

The definition of word is UA-dependent;
[[UAX29]] is suggested (but not required) for determining such word boundaries.

The '@text-transform/transform' descriptor may be used to refer
to existing text-transforms in the definition of a new one.
If the text-transforms referred to has a different position
than the position specified in the text-transform that refers to them,
they apply at the intersection of the two positions.

<div class=example>
<pre highlight=css>
@text-transform latin-only-uppercase {
	transform: "a-z" to "A-Z";
}
@text-transform latin-only-capitalize {
	transform: latin-only-uppercase;
	position: initial;
}
</pre>
</div>

<h2 id="dom">
DOM interaction</h2>

Custom text transformation values defined within @text-transform rules are accessible
via the following modifications to the CSS Object Model.

<pre class=idl>
partial interface CSSRule {
	const unsigned short TEXT_TRANSFORM_RULE = 1000;
};
interface CSSTextTransformRule : CSSRule {
	attribute          DOMString   name;
	readonly attribute CSSStyleDeclaration  style;
};
</pre>

Issue: This is a lousy OM. Do better.

<h2 class="no-num" id="use-cases">Apendix A.
Use cases and Examples</h2>
<em>This Apendix is non-normative</em>.

<h3 id=single-lang>
Single-Language uses</h3>

The following use cases only apply to a single language.
Defining all the possibly useful text-transforms for all languages
would go beyond the capacity and expertise of the CSS WG.
Having the generic mechanism allows authors to solve their specific problem.

<div class=example>
<h4 id=full-size-kana>
Full-size kana</h4>

In Japanese, small kana characters appearing within
<a>ruby</a> are sometimes replaced by the equivalent full-size kana for legibility purposes,
even though the semantically correct characters are used in the markup.
The following transformation defines this conversion.
<pre highlight=css>
@text-transform full-size-kana {
	transform: "ぁぃぅぇぉゕゖっゃゅょゎ" to "あいうえおかけつやゆよわ",
	           "ァィゥェォヵㇰヶㇱㇲッㇳㇴㇵㇶㇷㇸㇹㇺャュョㇻㇼㇽㇾㇿヮ" to "アイウエオカクケシスツトヌハヒフヘホムヤユヨラリルレロワ",
	           "ｧｨｩｪｫｯｬｭｮ" to "ｱｲｳｴｵﾂﾔﾕﾖ";
}
</pre>
</div>

<div class=example>
<h4 id=german-s>German ß</h4>

As discussed in [this email thread](http://lists.w3.org/Archives/Public/www-style/2011Nov/0193.html),
ß (aka &amp;szlig; or U+00DF) is traditionally considered a lower case letter
without an uppercase equivalent.
''text-transform: uppercase'' leaves it unchanged.
Unicode has introduced ẞ (U+1E9E),
an uppercase version of it since 5.1,
but without making it a target of toupper().

This letter being rather new,
authors are bound to disagree
whether it is a proper uppercase variant of U+00DF or not.
Those who think it is not may use ''text-transform: uppercase;'' and ''text-transform: lowercase''.
Those who think it is could use the following.

<pre highlight=css>
@text-transform german-uppercase {
	transform: U+00DF to U+1E9E, uppercase;
}

@text-transform german-lowercase {
	transform: U+1E9E to U+00DF, lowercase;
}
</pre>
<div class=issue>
 It has been suggested that overloading existing values with a language descriptor or selector would be better:

<pre highlight=css>
@text-transform uppercase {
	transform: U+00DF to U+1E9E;
	language: de;
}
</pre>
<pre highlight=css>
@text-transform uppercase:lang(de) {
	transform: U+00DF to U+1E9E;
}
</pre>
</div>
</div>

<div class=example>
<h4 id=turkish-i>Turkish i/ı</h4>

<a href="http://en.wikipedia.org/wiki/Dotted_and_dotless_I">In Turkish and a few related languages, dotted and dotless i are distinct letters, both in upper land lower case.</a>

The uppercasing and lowercasing algorithm defined for the text-transform property only preserve this when the content language of the element is known.

Someone may want to apply an uppercase or lowercase transformation to a document where language is insufficiently marked up, but known to the author of the style sheet to be Turkish. In this case, the generic uppercase and lowercase transformations would fail, but the following would work.

<pre highlight=css>
@text-transform turkic-uppercase {
	transform: "i" to "İ", uppercase;
}

@text-transform turkic-lowercase {
	transform: "I" to "ı", lowercase;
}
</pre>
</div>

<div class=example>
<h4 id=georgian>Georgian upper/lower case</h4>

The Georgian language has used <a href="http://en.wikipedia.org/wiki/Georgian_alphabet">three different unicameral alphabets</a> through history:
Asomtavruli, Nuskhuri, and Mkhedruli.
Some authors have been using Asomtavruli letters in an otherwise Mkhedruli text,
<a href="https://en.wikipedia.org/wiki/Georgian_scripts#Use_of_Asomtavruli_and_Nuskhuri_today">in a way that resembles a bicameral alphabet</a>.
The following transformations would be useful to authors
wishing to apply this effect to—or to remove it from—Georgian text.
<pre highlight=css>
@text-transform Mkhedruli-to-Asomtavruli {
	transform: "ა-ჵ" to "Ⴀ-Ⴥ";
}

@text-transform Asomtavruli-to-Mkhedruli {
	transform: "Ⴀ-Ⴥ" to "ა-ჵ";
}

@text-transform georgian-uppercase {
	transform: Mkhedruli-to-Asomtavruli;
}
@text-transform georgian-capitalize {
	transform: Mkhedruli-to-Asomtavruli;
	position: initial;
}
@text-transform georgian-lowercase {
	transform: Asomtavruli-to-Mkhedruli;
}
</pre>
</div>

<h3 id=cross-language>Cross-language uses</h3>
The following cases are examples of cases useful in several languages,
but rare enough that they are better addressed by authors when needed than by the CSS WG.

<div class=example>
<h4 id=long-s>Long s</h4>
In old (18th century and earlier) European texts,
the letter s, when at the middle or begining of the word,
<a href="http://en.wikipedia.org/wiki/Long_s">was written ſ</a> (<a href="http://www.fileformat.info/info/unicode/char/17f/index.htm">U+017F</a>).
S occuring at the end of a word would be written as the modern s is.

Modern readers are often unfamiliar with this letter form,
and for readability reasons,
one may want to convert from one to the other.
The follow transformation would accomplish this.

<pre highlight=css>
@text-transform modernize-s {
	transform: "ſ" to "s";
}
</pre>

This does the opposite transform:

<pre highlight=css>
@text-transform long-s {
	transform: "s" to "ſ" ;
	position: initial medial;
}
</pre>
</div>

<h3 id=misc>Miscellaneous</h3>
Here are some more example of how the generic mechanism may be used.

<div class=example>
<h4 id=asterix>Comic book Vikings</h4>

In the “Asterix and the Great Crossing” comic book,
the Viking characters are supposed to speak a foreign language
unintelligible to the main characters,
but still understandable to the readers.
This is represented by writing down their speech normally,
except that some letters are replaced by similarly looking letters found in Scandinavian languages.

This effect could be obtained by the following transform:

<pre highlight=css>
@text-transform fake-norse {
	transform: "aoAO" to "åøÅØ";
}
</pre>
</div>

<div class=example>
<h4 id=leet>L33t speak</h4>

In Internet, hacker and gamer culture,
a phenomenon is quite common where characters are replaced
by other characters or character sequences which have a somewhat similar appearance.
Although no single consensual convention exists
and sometimes mappings are neither injective nor surjective,
one could simulate this playful style with a transformation like the following:

<pre highlight=css>
@text-transform leet-speak {
	transform: "A-Z" to "48©)3F6H1!K£MN0¶9®57UVW*¥2";
}
</pre>
</div>

<div class=example>
<h4 id=rot13>Rot13</h4>
<pre highlight=css>
@text-transform rot13 {
	transform: "A-M" to "N-Z", "N-Z" to "A-M",
	           "a-m" to "n-z", "n-z" to "a-m";
}
.spoiler:hover {
    text-transform: rot13;
}
</pre>
<pre highlight=html>
&lt;p class="spoiler">Qnegu Inqbe vf Yhxr’f sngure.&lt;/p>
</pre>
</div>

<h2 class="no-num" id="security_and_privacy"> Apendix B.
Security and Privacy Considerations</h2>

<em>This appendix is non-normative</em>.

There are no known security or privacy impacts of this feature.

The W3C TAG is developing a
<a href="https://w3ctag.github.io/security-questionnaire/">Self-Review Questionnaire: Security and Privacy</a>
for editors of specifications to informatively answer.
As far as currently known, here are the answers to the <a href="https://w3ctag.github.io/security-questionnaire/#questions">Questions to Consider</a>:

<dl>
<dt>Does this specification deal with personally-identifiable information?
<dd>No

<dt>Does this specification deal with high-value data?
<dd>No

<dt>Does this specification introduce new state for an origin that persists across browsing sessions?
<dd>no

<dt>Does this specification expose any other data to an origin that it doesn’t currently have access to?
<dd>No

<dt>Does this specification enable new script execution/loading mechanisms?
<dd>No

<dt>Does this specification allow an origin access to a user’s location?
<dd>No

<dt>Does this specification allow an origin access to sensors on a user’s device?
<dd>No

<dt>Does this specification allow an origin access to aspects of a user’s local computing environment?
<dd>No

<dt>Does this specification allow an origin access to other devices?
<dd>No

<dt>Does this specification allow an origin some measure of control over a user agent’s native UI?
<dd>No.
However, it does allow changes to the text displayed
in some “native-looking” form controls and replaced elements within the page.
As these are under author control anyway, this is not considered a risk.

<dt>Does this specification expose temporary identifiers to the web?
<dd>No

<dt>Does this specification distinguish between behavior in first-party and third-party contexts?
<dd>No

<dt>How should this specification work in the context of a user agent’s "incognito" mode?
<dd>No difference in behavior is expected or needed.

<dt>Does this specification persist data to a user’s local device?
<dd>No

<dt>Does this specification have a "Security Considerations" and "Privacy Considerations" section?
<dd>Yes, this is the role of this Appendix.

<dt>Does this specification allow downgrading default security characteristics?
<dd>No
</dl>
