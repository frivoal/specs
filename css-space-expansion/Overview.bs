<h1>CSS space expansion</h1>

<pre class='metadata'>
ED: https://specs.rivoal.net/css-space-expansion/
Shortname: css-space-expansion
Level: 1
Status: UD
Group: csswg
Repository: https://gitlab.com/frivoal/specs.git gitlab
Issue Tracking: GitLab https://gitlab.com/frivoal/specs/issues
Issue Tracker Template: https://gitlab.com/frivoal/specs/issues/{0}
Work Status: Exploring
Editor: Florian Rivoal, https://florian.rivoal.net/
Abstract: This proposal explores a way to turn Zero Width Spaces into visible spaces.
	The driving use case is support for optional spacing in Japanese (known as “<span lang=ja>分かち書き</span>”)
	for the benefit of language learners and people with dyslexia.
	If adopted, this proposal is expected to be incorporated into [[CSS-TEXT-3]] or [[CSS-TEXT-4]].
</pre>

<h2 id=intro>
Introduction</h2>
<em>This section is non-normative</em>.

In a number of languages and writing system, such as Japanese or Thai,
words are not deliminated by spaces (or any other character)
as is the case in English (See <a href="https://www.w3.org/International/articles/typography/linebreak">Approaches to line breaking</a> for a discussion the approach various languages take to word separation and line breaking).

However, even if text without spaces is the dominant style in such languages,
there are cases where making word boundaries (or phrase boundaries) visible through the use of spaces
is desired.
This is a purely stylistic effect, with no implication on the semantics of the text.

In Japan for instance, this is commonly done in books for people learning the language--
young children or foreign students.
People with dyslexia also tend to find this style easier to read.
Recent pushes by the Japanese government
for electronic text-books have raised the demand for this type of features,
and proprietary ebook solutions are being proposed.

The mechanism proposed in this specification builds upon the existing use
of the <{wbr}> element
or of U+200B ZERO WIDTH SPACE
in the document markup as a word (or phrase) delimiter.
While this practice is not that common,
it is a semantically valid use of that unicode character,
and the ability to trigger stylistic effects based on it
can only encourage its use.

<h2 id=space-expansion>
Expanding Zero Width Spaces: the 'zero-width-space-expansion' property</h2>

<pre class="propdef">
Name: zero-width-space-expansion
Value: none | space | ideographic-space
Initial: none
Applies to: [=inline boxes=]
Inherited: yes
Percentages: N/A
Computed value: as specified
Animation type: discrete
</pre>

Issue: This name is too verbose. To be bikeshedded.

Issue: Should we allow more freeform values, like <<string>>, possibly limited to 1 character?

This property enables all instances of U+200B ZERO WIDTH SPACE to be replaced by the specified character.
Instances of <{wbr}> are considered equivalent to U+200B, and are also replaced.
This substitution happens before layout, so all layout operations that depend on the characters in the content (such as [[CSS-TEXT-3#white-space-rules]], [=line breaking=], or [=intrinsic sizing=]) must use that character instead of the original U+200B.

<dl dfn-for="zero-width-space-expansion" dfn-type="value">
	<dt><dfn>none</dfn>
	<dd>This property has no effect.

	<dt><dfn>space</dfn>
	<dd>All instances of U+200B ZERO WIDTH SPACE are replaced by U+0020 SPACE.

	<dt><dfn>ideographic-space</dfn>
	<dd>All instances of U+200B ZERO WIDTH SPACE are replaced by U+3000 IDEOGRAPHIC SPACE.
</dl>

Like 'text-transform', this property transforms text for styling purposes.
It has no effect on the underlying content,
and must not affect the content of a plain text copy & paste operation.

<div class=issue>
This almost looks like instead of a new property,
we could just add two new values of the 'text-transform' property.
However:
* this property seems saner to handle before [[CSS-TEXT-3#white-space-rules]],
	but 'text-transform' happens after that.
	Or maybe which stage 'text-transform' applies at depends on which value it uses?
* these two properties seems better to cascade separately

</div>

<div class=example id=wakachigaki>
	<style>
	#wakachigaki samp 
	{
		max-width: 18em;
		line-height: 2;
		padding: 1em;
		display: block;
		margin: auto;
		border: solid gray 1px;
		background: white;
	}
	</style>

	Unlike books for adults, Japanese books for young children often feature spaces between sentence segments,
	to facilitate reading.

	Absent any particular styling, the following sentence would be rendered as depicted below.

	<pre><code highlight=markup>
	&lt;p>むかしむかし、&lt;wbr>あるところに、&lt;wbr>おじいさんと&lt;wbr>おばあさんが&lt;wbr>すんでいました。
	</code></pre>

	<samp lang=ja>
	むかしむかし、あるところに、おじいさんとおばあさんがすんでいました。
	</samp>

	<hr>

	Phrase-based spacing can be achieved with the following css:

	<pre><code highlight=css>
	p {
		zero-width-space-expansion: ideographic-space;
	}
	</code></pre>

	<samp lang=ja>
	むかしむかし、　あるところに、　おじいさんと　おばあさんが　すんでいました。
	</samp>

	<hr>

	Another common variant additionally restricts the allowable line breaks to these phrase boundaries.
	Using the same markup, this is easily achieved with the following css:

	<pre><code highlight=css>
	p {
		word-break: keep-all;
		zero-width-space-expansion: ideographic-space;
	}
	</code></pre>

	<samp style="word-break:keep-all" lang=ja>
	むかしむかし、　<wbr>あるところに、　<wbr>おじいさんと　<wbr>おばあさんが　<wbr>すんでいました。
	</samp>

</div>

<div class=example id=classes>
	<style>
	#classes samp 
	{
		max-width: 18em;
		line-height: 2;
		padding: 1em;
		display: block;
		margin: auto;
		border: solid gray 1px;
		background: white;
		text-align: left;
	}
	</style>

	In addition to making the source code more readable,
	using <{wbr}> rather than U+200B in the markup
	also allow authors to classify the delimiters into different groups.

	In the following example,
	<{wbr}> elements are either
	unmarked when they delimit a word,
	or marked with class <code>p</code> when they also delimit a phrase.

	<pre><code highlight=markup>
	&lt;p>らいしゅう&lt;wbr>の&lt;wbr>じゅぎょう&lt;wbr>に&lt;wbr class=p
	>たいこ&lt;wbr>と&lt;wbr>ばち&lt;wbr>を&lt;wbr class=p
	>もって&lt;wbr>きて&lt;wbr>ください。
	</code></pre>

	Using this, it is possible not only to enable the rather common phrase based spacing,
	but also word by word spacing that is likely to be preferred by people with dyslexia to reduce ambiguities,
	or other variants such as a combination of phrase-based spacing and of word-based wrapping.

	<figure>
		<figcaption>Usual rendering</figcaption>

		<samp>
		らいしゅう<wbr>の<wbr>じゅぎょう<wbr>に<wbr class=p>たいこ<wbr>と<wbr>ばち<wbr>を<wbr class=p>もって<wbr>きて<wbr>ください。
		</samp>
	</figure>
	<hr>
	<figure>
		<figcaption>Phrase spacing</figcaption>

		<pre><code highlight=css>
		p wbr.p {
			zero-width-space-expansion: ideographic-space;
		}
		</code></pre>

		<samp>
		らいしゅう<wbr>の<wbr>じゅぎょう<wbr>に　<wbr class=p>たいこ<wbr>と<wbr>ばち<wbr>を　<wbr class=p>もって<wbr>きて<wbr>ください。
		</samp>
	</figure>
	<hr>
	<figure>
		<figcaption>Word spacing</figcaption>

		<pre><code highlight=css>
		p wbr {
			zero-width-space-expansion: ideographic-space;
		}
		</code></pre>

		<samp>
		らいしゅう　<wbr>の　<wbr>じゅぎょう　<wbr>に　<wbr class=p>たいこ　<wbr>と　<wbr>ばち　<wbr>を　<wbr class=p>もって　<wbr>きて　<wbr>ください。
		</samp>
	</figure>
	<hr>
	<figure>
		<figcaption>Phrase spacing, word wrapping</figcaption>

		<pre><code highlight=css>
		p {
			word-break: keep-all;
		}
		p wbr.p {
			zero-width-space-expansion: ideographic-space;
		}
		</code></pre>

		<samp style="word-break: keep-all">
		らいしゅう<wbr>の<wbr>じゅぎょう<wbr>に　<wbr class=p>たいこ<wbr>と<wbr>ばち<wbr>を　<wbr class=p>もって<wbr>きて<wbr>ください。
		</samp>
	</figure>
	<hr>
	<figure>
		<figcaption>Word spacing and wrapping</figcaption>

		<pre><code highlight=css>
		p {
			word-break: keep-all;
		}
		p wbr {
			zero-width-space-expansion: ideographic-space;
		}
		</code></pre>

		<samp style="word-break: keep-all">
		らいしゅう　<wbr>の　<wbr>じゅぎょう　<wbr>に　<wbr class=p>たいこ　<wbr>と　<wbr>ばち　<wbr>を　<wbr class=p>もって　<wbr>きて　<wbr>ください。
		</samp>
	</figure>

</div>
<h2 class="no-num" id="security_and_privacy"> Apendix A. 
Security and Privacy Considerations</h2>

<em>This appendix is non-normative</em>.

There are no known security or privacy impacts of this feature. 

The W3C TAG is developing a
<a href="https://w3ctag.github.io/security-questionnaire/">Self-Review Questionnaire: Security and Privacy</a>
for editors of specifications to informatively answer.
As far as currently known, here are the answers to the <a href="https://w3ctag.github.io/security-questionnaire/#questions">Questions to Consider</a>:

<dl>
<dt>Does this specification deal with personally-identifiable information?
<dd>No

<dt>Does this specification deal with high-value data?
<dd>No.

<dt>Does this specification introduce new state for an origin that persists across browsing sessions?
<dd>No.

<dt>Does this specification expose any other data to an origin that it doesn’t currently have access to?
<dd>No.

<dt>Does this specification enable new script execution/loading mechanisms?
<dd>No.

<dt>Does this specification allow an origin access to a user’s location?
<dd>No.

<dt>Does this specification allow an origin access to sensors on a user’s device?
<dd>No.

<dt>Does this specification allow an origin access to aspects of a user’s local computing environment?
<dd>No.

<dt>Does this specification allow an origin access to other devices?
<dd>No.

<dt>Does this specification allow an origin some measure of control over a user agent’s native UI?
<dd>No.

<dt>Does this specification expose temporary identifiers to the web?
<dd>No.

<dt>Does this specification distinguish between behavior in first-party and third-party contexts?
<dd>No.

<dt>How should this specification work in the context of a user agent’s "incognito" mode?
<dd>No difference in behavior is expected or needed.

<dt>Does this specification persist data to a user’s local device?
<dd>No.

<dt>Does this specification have a "Security Considerations" and "Privacy Considerations" section?
<dd>Yes, this is the role of this Appendix.

<dt>Does this specification allow downgrading default security characteristics?
<dd>No.
</dl>

